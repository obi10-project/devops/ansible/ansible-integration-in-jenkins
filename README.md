##### Ansible integration in Jenkins

#### The objective of the project is to create a Jenkinsfile that calls an Ansible playbook execution on a remote server. 
#### The playbook will provision 2 EC2 instances by installing docker and docker compose.

#### 1. Integrate Ansible in a Jenkins pipeline. 
#### 2. Execute Ansible playbooks in a Jenkins pipeline.


